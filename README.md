# FastTree

Implementation of the FastTree algorithm (https://academic.oup.com/mbe/article/26/7/1641/1128976) for the course CS4255.

## How to run

Running main.py in the src folder will write the phylogenetic tree corresponding to the input file in newick format to fasttree-newick-output.txt, which can be found in the data folder.
The input file can be changed by altering the argument for the initialize_sequences function, which is called inside the run function.

## Contributors

Caroline Freyer 5367972  
Francesca Drummer 5413990  
Pia Keukeleire 4550676  
